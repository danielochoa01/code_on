import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Respuesta } from '../interfaces/respuesta';

@Injectable()
export class AutenticacionService {

  private estadoLogueado = false;

  constructor(private http: HttpClient) { }

  setEstadoLogueado(value: boolean) {
    this.estadoLogueado = value;
  }

  get estaLogueado() {
    return this.estadoLogueado;
  }

  login(correo, contrasenia) {
    return this.http.post<Respuesta>('http://localhost:3000/api/login', {
      correo,
      contrasenia,
    }, {
        withCredentials: true
    });
  }

  crearUsuario(
    nombreCompleto: string,
    fechaNacimiento: string,
    nombreUsuario: string,
    correo: string,
    contrasenia: string,
    urlFoto: string,
    genero: string,
    fechaRegistro: string,
    idPlan: number,
    estaActivo: boolean
  ) {
    return this.http.post<Respuesta>('http://localhost:3000/api/register', {
      nombreCompleto,
      fechaNacimiento,
      nombreUsuario,
      correo,
      contrasenia,
      urlFoto,
      genero,
      fechaRegistro,
      idPlan,
      estaActivo
    }, { withCredentials: true }
    );
  }
}
