import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../interfaces/usuario';
import { EstadoLogueado } from '../interfaces/estado-logueado';
import { LogoutStatus } from '../interfaces/logout-status';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) { }

  estaLogueado() {
    return this.http.get<EstadoLogueado>('http://localhost:3000/api/esta-logueado',
    { withCredentials: true});
  }

  obtenerInformacionUsuario() {
    return this.http.get<Usuario>('http://localhost:3000/api/usuario',
    { withCredentials: true });
  }

  logout() {
    return this.http.get<LogoutStatus>('http://localhost:3000/api/logout',
    { withCredentials: true });
  }
}
