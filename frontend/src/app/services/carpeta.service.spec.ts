import { TestBed, inject } from '@angular/core/testing';

import { CarpetaService } from './carpeta.service';

describe('CarpetaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CarpetaService]
    });
  });

  it('should be created', inject([CarpetaService], (service: CarpetaService) => {
    expect(service).toBeTruthy();
  }));
});
