import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Carpeta } from '../interfaces/carpeta';
import { Respuesta } from '../interfaces/respuesta';

@Injectable({
  providedIn: 'root'
})
export class CarpetaService {

  constructor(private http: HttpClient) { }

  crearCarpeta(
    id_proyecto,
    nombre_carpeta,
    fecha_creacion,
    fecha_modificacion,
    url_carpeta,
    id_carpeta_padre,
    id_usuario_creador,
    usuarios) {
    return this.http.post<Respuesta>('http://localhost:3000/api/directory',
    {
      id_proyecto,
      nombre_carpeta,
      fecha_creacion,
      fecha_modificacion,
      url_carpeta,
      id_carpeta_padre,
      id_usuario_creador,
      usuarios
    },
    { withCredentials: true });
  }

}
