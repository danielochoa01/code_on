import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Archivo } from '../interfaces/archivo';
import { Respuesta } from '../interfaces/respuesta';
import { CodigoArchivo } from '../interfaces/codigo-archivo';

@Injectable({
  providedIn: 'root'
})
export class ArchivoService {

  constructor(private http: HttpClient) { }

  crearArchivo(
    nombre_archivo,
    id_tipo_archivo,
    contenido,
    fecha_creacion,
    fecha_modificacion,
    id_carpeta_contenedora,
    id_proyecto,
    id_usuario_creador,
    usuarios) {
    return this.http.post<Respuesta>('http://localhost:3000/api/file',
      {
        nombre_archivo,
        id_tipo_archivo,
        contenido,
        fecha_creacion,
        fecha_modificacion,
        id_carpeta_contenedora,
        id_proyecto,
        id_usuario_creador,
        usuarios
      },
      { withCredentials: true });
  }

  actualizarArchivo(
    id_archivo,
    id_tipo_archivo,
    contenido,
    fecha_modificacion
  ) {
    return this.http.post<Respuesta>('http://localhost:3000/api/actualizar',
      {
        id_archivo,
        id_tipo_archivo,
        contenido,
        fecha_modificacion
      },
      { withCredentials: true });
  }
  obtenerInformacionArchivos() {
    return this.http.get<Archivo[]>('http://localhost:3000/api/file', { withCredentials: true });
  }

  obtenerArchivo(id_archivo) {
    return this.http.post<CodigoArchivo>('http://localhost:3000/api/editor', {
      id_archivo
    }, { withCredentials: true });
  }
}
