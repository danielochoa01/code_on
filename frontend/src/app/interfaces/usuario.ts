export interface Usuario {
    _id?: number;
    nombre_completo?: string;
    fecha_nacimiento?: string;
    nombre_usuario?: string;
    correo?: string;
    contrasenia?: string;
    url_foto?: string;
    genero?: string;
    fecha_registro?: string;
    id_plan?: number;
    esta_activo?: boolean; // Esta activo o inactivo
}
