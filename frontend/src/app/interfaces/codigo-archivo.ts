export interface CodigoArchivo {
    status: Number;
    mensaje: String;
    lenguaje: String;
}
