export interface EstadoLogueado {
    estaLogueado: Boolean;
    mensaje: String;
}
