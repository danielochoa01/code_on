export interface Carpeta {
    _id?: Number;
    id_proyecto?: Number ; // id proyecto
    nombre_carpeta?: String;
    fecha_creacion?: Date;
    fecha_modificacion?: Date;
    url_carpeta?: String;
    id_carpeta_padre?: Number;
    id_usuario_creador?: Number;
    usuarios?: [
        Number
    ];
}
