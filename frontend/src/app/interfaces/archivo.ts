export interface Archivo {
    _id?: Number;
    nombre_archivo?: String;
    id_tipo_archivo?: Number;
    tipo_archivo?: String;
    fecha_creacion?: Date;
    fecha_modificacion?: Date;
    tamanio_bytes?: Number;
    url_archivo?: String;
    id_carpeta_contenedora?: Number;
    id_proyecto?: Number;
    id_usuario_creador?: Number;
    usuarios?: [
      Number
    ];
}
