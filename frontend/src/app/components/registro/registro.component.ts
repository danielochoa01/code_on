import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavbarService } from '../../services/navbar.service';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Usuario } from '../../interfaces/usuario';
import { AutenticacionService } from '../../services/autenticacion.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.scss']
})
export class RegistroComponent implements OnInit {

  usuarioForm: FormGroup;
  nombreCompletoUsuarioForm: FormControl;
  fechaNacimientoForm: FormControl;
  nombreUsuarioForm: FormControl;
  correoForm: FormControl;
  contraseniaForm: FormControl;
  generoForm: FormControl;
  correoExiste: Boolean;
  usuarioExiste: Boolean;

  usuario: Usuario = {
    _id: null,
    nombre_completo: '',
    fecha_nacimiento: '',
    nombre_usuario: '',
    correo: '',
    contrasenia: '',
    genero: '',
    fecha_registro: '',
    id_plan: 1,
    esta_activo: null
  };

  constructor(
    private autenticacion: AutenticacionService,
    private router: Router,
    private nav: NavbarService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.nav.hide();

    this.nombreCompletoUsuarioForm = new FormControl(this.usuario.nombre_completo, [
      Validators.required,
      Validators.minLength(8),
      ]
    );
    this.fechaNacimientoForm = new FormControl(this.usuario.fecha_nacimiento, Validators.required);
    this.nombreUsuarioForm = new FormControl(this.usuario.nombre_usuario, [
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.-]*$')
      ]
    );
    this.correoForm =  new FormControl(this.usuario.correo, [
      Validators.required,
      Validators.pattern('[^ @]*@[^ @]*')
      ]
    );
    this.contraseniaForm = new FormControl(this.usuario.contrasenia, [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(16),
      Validators.pattern('(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}')
      ]
    ),
    this.generoForm = new FormControl(this.usuario.genero, Validators.required);

    this.usuarioForm = this.formBuilder.group({
      nombreCompleto: this.nombreCompletoUsuarioForm,
      fechaNacimiento: this.fechaNacimientoForm,
      nombreUsuario: this.nombreUsuarioForm,
      correo: this.correoForm,
      contrasenia: this.contraseniaForm,
      genero: this.generoForm
    });
  }

  crearUsuario(event) {
    event.preventDefault();

    const meses = ['01', '02', '03', '04', '05', '06',
      '07', '08', '09', '10', '11', '12'
    ];
    const nombre = this.usuarioForm.value.nombreCompleto;
    const fechaNacimiento = this.usuarioForm.value.fechaNacimiento;
    const nombreUsuario = this.usuarioForm.value.nombreUsuario;
    const correo = this.usuarioForm.value.correo;
    const contrasenia = this.usuarioForm.value.contrasenia;
    const genero = this.usuarioForm.value.genero;
    const urlFoto = 'http://localhost:3000/profile_pics/profile.jpg';
    const id_plan = 1;
    const esta_activo = true;

    const fecha = new Date();

    const fechaActual = `${fecha.getFullYear()}-${meses[fecha.getMonth()]}-${fecha.getDate()}`;

    if (!this.usuarioForm.valid) {
      console.log(this.usuarioForm.value);
    } else {
      console.log(this.usuarioForm.value.nombreCompleto);
      this.autenticacion.crearUsuario(
        nombre,
        fechaNacimiento,
        nombreUsuario,
        correo,
        contrasenia,
        urlFoto,
        genero,
        fechaActual,
        id_plan,
        esta_activo
      ).subscribe( (resultado) => {
          console.log(resultado);
            if (resultado.status === 200) {

              alert(resultado.mensaje);
              this.router.navigate(['/login']);
              this.correoExiste = false;
              this.usuarioExiste = false;

            } else if (resultado.status === 501) {
              alert(resultado.mensaje);
              this.correoExiste = true;
              this.usuarioExiste = false;
            } else if (resultado.status === 502) {
              alert(resultado.mensaje);
              this.usuarioExiste = true;
              this.correoExiste = false;
            }
          }
        );
    }
  }
}
