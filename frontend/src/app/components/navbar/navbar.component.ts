import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { UsuarioService } from '../../services/usuario.service';
import { AutenticacionService } from '../../services/autenticacion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public userName: string;
  public userEmail: string;
  public userPicture: string;
  public userId: string;
  public isLogin: boolean;
  public busqueda: string;

  constructor(
    public nav: NavbarService,
    private router: Router,
    private usuarioService: UsuarioService,
    private autenticacion: AutenticacionService
  ) { }

  ngOnInit() {
    this.comprobarUserLogin();
  }

  comprobarUserLogin() {
    this.usuarioService.estaLogueado().subscribe(
      auth => {
        console.log(auth);
        if (auth.estaLogueado) {
          this.isLogin = true;
          this.usuarioService.obtenerInformacionUsuario().
            subscribe(
              respuesta => {
                console.log(respuesta.url_foto);
                this.userName = respuesta.nombre_usuario;
                this.userEmail = respuesta.correo;
                this.userPicture = respuesta.url_foto;
              }
            );
        } else {
          this.isLogin = false;
        }
      }
    );
  }

  search(contenidoBusqueda) {
    console.log(`Se hará una búsqueda de ${contenidoBusqueda}`);
  }

  logout() {
    this.usuarioService.logout().
    subscribe(
      respuesta => {
        console.log(respuesta.mensaje);
        this.router.navigate(['/login']);
      }
    );
  }
}
