import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { AutenticacionService } from '../../services/autenticacion.service';
import { UsuarioService } from '../../services/usuario.service';
import { CarpetaService } from '../../services/carpeta.service';
import { ArchivoService } from '../../services/archivo.service';
import { Archivo } from '../../interfaces/archivo';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // nombreArchivoModal = new FormControl('', Validators.required);

  idUsuario: Number;
  nombreCarpeta: String;
  fechaCreacion: String;
  fechaModificacion: String;
  urlCarpeta: String;
  idCarpetaPadre: Number;
  idUsuarioCreador: Number;
  usuarios: Array<Number>;
  archivos = [];

  constructor(
    private router: Router,
    private nav: NavbarService,
    private autenticacion: AutenticacionService,
    private usuario: UsuarioService,
    private carpeta: CarpetaService,
    private archivo: ArchivoService
  ) { }

  ngOnInit() {
    this.nav.show();

    console.log(this.autenticacion.estaLogueado);

    this.usuario.estaLogueado().
    subscribe(
      (respuesta) => {
        console.log(respuesta.estaLogueado);
      }
    );

    this.usuario.obtenerInformacionUsuario().
      subscribe(
        (respuesta) => {
          this.idUsuario = respuesta._id;
        }
      );

    this.archivo.obtenerInformacionArchivos().
      subscribe(
        (respuesta) => {
          this.archivos = respuesta;
          console.log(respuesta);
          console.log(this.archivos);
        }
    );


  }

  irAEditor(nombreArchivo) {
    this.router.navigate(['/editor/:', { nombre: nombreArchivo, id: 0 }]);
    console.log(nombreArchivo);
  }

  irAEditorId(idArchivo) {
    this.router.navigate(['/editor/:', { id: idArchivo, nombre: '' }]);
    console.log(idArchivo);
  }

  crearCarpeta(
    nombreCarpeta
  ) {

    console.log('Click');
    console.log(this.idUsuario);

    const meses = ['01', '02', '03', '04', '05', '06',
      '07', '08', '09', '10', '11', '12'
    ];

    const fecha = new Date();

    const fechaActual = `${fecha.getFullYear()}-${meses[fecha.getMonth()]}-${fecha.getDate()}`;
    const idProyecto = 0;
    const nombreCarpetaCreada = nombreCarpeta;
    const fechaCreacion = fechaActual;
    const fechaModificacion = fechaActual;
    const urlCarpeta = `/${nombreCarpeta}`;
    const idCarpetaPadre = 1;
    const idUsuarioCreador = this.idUsuario;
    const usuarios = 0;

    this.carpeta.crearCarpeta(
      idProyecto,
      nombreCarpetaCreada,
      fechaCreacion,
      fechaModificacion,
      urlCarpeta,
      idCarpetaPadre,
      idUsuarioCreador,
      usuarios).subscribe(
        (respuesta) => {
          if (respuesta.status === 200 ) {
            alert(respuesta.mensaje);
          }
        }
      );
  }
}
