import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavbarService } from '../../services/navbar.service';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Usuario } from '../../interfaces/usuario';
import { AutenticacionService } from '../../services/autenticacion.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  usuarioForm: FormGroup;
  correoForm: FormControl;
  contraseniaForm: FormControl;

  constructor(
    private router: Router,
    private nav: NavbarService,
    private formBuilder: FormBuilder,
    private autenticacion: AutenticacionService
  ) { }

  ngOnInit() {
    this.nav.hide();
    this.correoForm = new FormControl('', [
      Validators.required,
      Validators.pattern('[^ @]*@[^ @]*')
      ]
    );
    this.contraseniaForm = new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(16),
      Validators.pattern('(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}')
      ]
    );

    this.usuarioForm = this.formBuilder.group({
      correo: this.correoForm,
      contrasenia: this.contraseniaForm,
    });
  }

  login(event) {
    event.preventDefault();

    const correo = this.usuarioForm.value.correo;
    const contrasenia = this.usuarioForm.value.contrasenia;

    if (!this.usuarioForm.valid) {
      console.log(this.usuarioForm.value);
    } else {
      this.autenticacion.login(correo, contrasenia)
      .subscribe(
        (respuesta) => {
          console.log(respuesta);
          if (respuesta.status === 200) {
            this.autenticacion.setEstadoLogueado(true);
            this.router.navigate(['/']);
          }
        }
      );
    }
  }

}
