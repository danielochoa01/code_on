import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavbarService } from '../../services/navbar.service';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {

  constructor(
    private router: Router,
    private nav: NavbarService,
    private usuarioService: UsuarioService
  ) { }

  ngOnInit() {
    this.nav.show();
    this.usuarioService.obtenerInformacionUsuario().
    subscribe(
      respuesta => {
        console.log(respuesta.nombre_usuario);
      }
    );
  }
}
