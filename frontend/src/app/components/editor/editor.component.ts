import { Component, OnInit } from '@angular/core';
import { saveAs } from 'file-saver';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { NavbarService } from '../../services/navbar.service';
import { ArchivoService } from '../../services/archivo.service';
import { UsuarioService } from '../../services/usuario.service';

/*
* Import de todos los temas que posee Ace
*/

import 'brace/theme/ambiance';
import 'brace/theme/chaos';
import 'brace/theme/chrome';
import 'brace/theme/clouds';
import 'brace/theme/clouds_midnight';
import 'brace/theme/cobalt';
import 'brace/theme/crimson_editor';
import 'brace/theme/dawn';
import 'brace/theme/dreamweaver';
import 'brace/theme/eclipse';
import 'brace/theme/github';
import 'brace/theme/idle_fingers';
import 'brace/theme/iplastic';
import 'brace/theme/katzenmilch';
import 'brace/theme/kr_theme';
import 'brace/theme/kuroir';
import 'brace/theme/merbivore';
import 'brace/theme/merbivore_soft';
import 'brace/theme/mono_industrial';
import 'brace/theme/monokai';
import 'brace/theme/pastel_on_dark';
import 'brace/theme/solarized_dark';
import 'brace/theme/solarized_light';
import 'brace/theme/sqlserver';
import 'brace/theme/terminal';
import 'brace/theme/textmate';
import 'brace/theme/tomorrow';
import 'brace/theme/tomorrow_night';
import 'brace/theme/tomorrow_night_blue';
import 'brace/theme/tomorrow_night_bright';
import 'brace/theme/tomorrow_night_eighties';
import 'brace/theme/twilight';
import 'brace/theme/vibrant_ink';
import 'brace/theme/xcode';

/*
* Import de todos los lenguajes que posee Ace
*/

import 'brace/mode/abap';
import 'brace/mode/abc';
import 'brace/mode/actionscript';
import 'brace/mode/ada';
import 'brace/mode/apache_conf';
import 'brace/mode/asciidoc';
import 'brace/mode/assembly_x86';
import 'brace/mode/autohotkey';
import 'brace/mode/batchfile';
import 'brace/mode/c9search';
import 'brace/mode/c_cpp';
import 'brace/mode/cirru';
import 'brace/mode/clojure';
import 'brace/mode/cobol';
import 'brace/mode/coffee';
import 'brace/mode/coldfusion';
import 'brace/mode/csharp';
import 'brace/mode/css';
import 'brace/mode/curly';
import 'brace/mode/d';
import 'brace/mode/dart';
import 'brace/mode/diff';
import 'brace/mode/dockerfile';
import 'brace/mode/dot';
import 'brace/mode/eiffel';
import 'brace/mode/ejs';
import 'brace/mode/elixir';
import 'brace/mode/elm';
import 'brace/mode/erlang';
import 'brace/mode/forth';
import 'brace/mode/ftl';
import 'brace/mode/gcode';
import 'brace/mode/gherkin';
import 'brace/mode/gitignore';
import 'brace/mode/glsl';
import 'brace/mode/golang';
import 'brace/mode/groovy';
import 'brace/mode/haml';
import 'brace/mode/handlebars';
import 'brace/mode/haskell';
import 'brace/mode/haxe';
import 'brace/mode/html';
import 'brace/mode/html_ruby';
import 'brace/mode/ini';
import 'brace/mode/io';
import 'brace/mode/jack';
import 'brace/mode/jade';
import 'brace/mode/java';
import 'brace/mode/javascript';
import 'brace/mode/json';
import 'brace/mode/jsoniq';
import 'brace/mode/jsp';
import 'brace/mode/jsx';
import 'brace/mode/julia';
import 'brace/mode/latex';
import 'brace/mode/less';
import 'brace/mode/liquid';
import 'brace/mode/lisp';
import 'brace/mode/livescript';
import 'brace/mode/logiql';
import 'brace/mode/lsl';
import 'brace/mode/lua';
import 'brace/mode/luapage';
import 'brace/mode/lucene';
import 'brace/mode/makefile';
import 'brace/mode/markdown';
import 'brace/mode/mask';
import 'brace/mode/matlab';
import 'brace/mode/mel';
import 'brace/mode/mushcode';
import 'brace/mode/mysql';
import 'brace/mode/nix';
import 'brace/mode/objectivec';
import 'brace/mode/ocaml';
import 'brace/mode/pascal';
import 'brace/mode/perl';
import 'brace/mode/pgsql';
import 'brace/mode/php';
import 'brace/mode/powershell';
import 'brace/mode/praat';
import 'brace/mode/prolog';
import 'brace/mode/properties';
import 'brace/mode/protobuf';
import 'brace/mode/python';
import 'brace/mode/r';
import 'brace/mode/rdoc';
import 'brace/mode/rhtml';
import 'brace/mode/ruby';
import 'brace/mode/rust';
import 'brace/mode/sass';
import 'brace/mode/scad';
import 'brace/mode/scala';
import 'brace/mode/scheme';
import 'brace/mode/scss';
import 'brace/mode/sh';
import 'brace/mode/sjs';
import 'brace/mode/smarty';
import 'brace/mode/snippets';
import 'brace/mode/soy_template';
import 'brace/mode/space';
import 'brace/mode/sql';
import 'brace/mode/stylus';
import 'brace/mode/svg';
import 'brace/mode/tcl';
import 'brace/mode/tex';
import 'brace/mode/text';
import 'brace/mode/textile';
import 'brace/mode/toml';
import 'brace/mode/twig';
import 'brace/mode/typescript';
import 'brace/mode/vala';
import 'brace/mode/vbscript';
import 'brace/mode/velocity';
import 'brace/mode/verilog';
import 'brace/mode/vhdl';
import 'brace/mode/xml';
import 'brace/mode/xquery';
import 'brace/mode/yaml';

declare let ace: any;

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  idArchivo: Number;
  language: string;
  theme: string;
  fontSize: string;
  text: String;
  idUsuario: Number;
  nombreArchivo: string;
  idTipoArchivo: Number;
  fechaCreacion: String;
  fechaModificacion: String;
  idCarpetaContenedora: String;
  idProyecto: Number;
  idUsuarioCreador: Number;
  esEditable: Boolean;
  usuarios: Array<Number>;

  languages = [
    {
      'name': 'ABAP',
      'value': 'abap'
    },
    {
      'name': 'ABC',
      'value': 'abc'
    },
    {
      'name': 'ActionScript',
      'value': 'actionscript'
    },
    {
      'name': 'ADA',
      'value': 'ada'
    },
    {
      'name': 'Apache_Conf',
      'value': 'apache_conf'
    },
    {
      'name': 'AsciiDoc',
      'value': 'asciidoc'
    },
    {
      'name': 'Assembly_x86',
      'value': 'assembly_x86'
    },
    {
      'name': 'AutoHotKey',
      'value': 'autohotkey'
    },
    {
      'name': 'BatchFile',
      'value': 'batchfile'
    },
    {
      'name': 'C9Search',
      'value': 'c9search'
    },
    {
      'name': 'C_Cpp',
      'value': 'c_cpp'
    },
    {
      'name': 'Cirru',
      'value': 'cirru'
    },
    {
      'name': 'Clojure',
      'value': 'clojure'
    },
    {
      'name': 'Cobol',
      'value': 'cobol'
    },
    {
      'name': 'coffee',
      'value': 'coffee'
    },
    {
      'name': 'ColdFusion',
      'value': 'coldfusion'
    },
    {
      'name': 'CSharp',
      'value': 'csharp'
    },
    {
      'name': 'CSS',
      'value': 'css'
    },
    {
      'name': 'Curly',
      'value': 'curly'
    },
    {
      'name': 'D',
      'value': 'd'
    },
    {
      'name': 'Dart',
      'value': 'dart'
    },
    {
      'name': 'Diff',
      'value': 'diff'
    },
    {
      'name': 'Dockerfile',
      'value': 'dockerfile'
    },
    {
      'name': 'Dot',
      'value': 'dot'
    },
    {
      'name': 'Dummy',
      'value': 'dummy'
    },
    {
      'name': 'DummySyntax',
      'value': 'dummysyntax'
    },
    {
      'name': 'Eiffel',
      'value': 'eiffel'
    },
    {
      'name': 'EJS',
      'value': 'ejs'
    },
    {
      'name': 'Elixir',
      'value': 'elixir'
    },
    {
      'name': 'Elm',
      'value': 'elm'
    },
    {
      'name': 'Erlang',
      'value': 'erlang'
    },
    {
      'name': 'Forth',
      'value': 'forth'
    },
    {
      'name': 'FTL',
      'value': 'ftl'
    },
    {
      'name': 'Gcode',
      'value': 'gcode'
    },
    {
      'name': 'Gherkin',
      'value': 'gherkin'
    },
    {
      'name': 'Gitignore',
      'value': 'gitignore'
    },
    {
      'name': 'Glsl',
      'value': 'glsl'
    },
    {
      'name': 'golang',
      'value': 'golang'
    },
    {
      'name': 'Groovy',
      'value': 'groovy'
    },
    {
      'name': 'HAML',
      'value': 'haml'
    },
    {
      'name': 'Handlebars',
      'value': 'handlebars'
    },
    {
      'name': 'Haskell',
      'value': 'haskell'
    },
    {
      'name': 'haXe',
      'value': 'haxe'
    },
    {
      'name': 'HTML',
      'value': 'html'
    },
    {
      'name': 'HTML_Ruby',
      'value': 'html_ruby'
    },
    {
      'name': 'INI',
      'value': 'ini'
    },
    {
      'name': 'Io',
      'value': 'io'
    },
    {
      'name': 'Jack',
      'value': 'jack'
    },
    {
      'name': 'Jade',
      'value': 'jade'
    },
    {
      'name': 'Java',
      'value': 'java'
    },
    {
      'name': 'JavaScript',
      'value': 'javascript'
    },
    {
      'name': 'JSON',
      'value': 'json'
    },
    {
      'name': 'JSONiq',
      'value': 'jsoniq'
    },
    {
      'name': 'JSP',
      'value': 'jsp'
    },
    {
      'name': 'JSX',
      'value': 'jsx'
    },
    {
      'name': 'Julia',
      'value': 'julia'
    },
    {
      'name': 'LaTeX',
      'value': 'latex'
    },
    {
      'name': 'LESS',
      'value': 'less'
    },
    {
      'name': 'Liquid',
      'value': 'liquid'
    },
    {
      'name': 'Lisp',
      'value': 'lisp'
    },
    {
      'name': 'LiveScript',
      'value': 'livescript'
    },
    {
      'name': 'LogiQL',
      'value': 'logiql'
    },
    {
      'name': 'LSL',
      'value': 'lsl'
    },
    {
      'name': 'Lua',
      'value': 'lua'
    },
    {
      'name': 'LuaPage',
      'value': 'luapage'
    },
    {
      'name': 'Lucene',
      'value': 'lucene'
    },
    {
      'name': 'Makefile',
      'value': 'makefile'
    },
    {
      'name': 'Markdown',
      'value': 'markdown'
    },
    {
      'name': 'Mask',
      'value': 'mask'
    },
    {
      'name': 'MATLAB',
      'value': 'matlab'
    },
    {
      'name': 'MEL',
      'value': 'mel'
    },
    {
      'name': 'MUSHCode',
      'value': 'mushcode'
    },
    {
      'name': 'MySQL',
      'value': 'mysql'
    },
    {
      'name': 'Nix',
      'value': 'nix'
    },
    {
      'name': 'ObjectiveC',
      'value': 'objectivec'
    },
    {
      'name': 'OCaml',
      'value': 'ocaml'
    },
    {
      'name': 'Pascal',
      'value': 'pascal'
    },
    {
      'name': 'Perl',
      'value': 'perl'
    },
    {
      'name': 'pgSQL',
      'value': 'pgsql'
    },
    {
      'name': 'PHP',
      'value': 'php'
    },
    {
      'name': 'Powershell',
      'value': 'powershell'
    },
    {
      'name': 'Praat',
      'value': 'praat'
    },
    {
      'name': 'Prolog',
      'value': 'prolog'
    },
    {
      'name': 'Properties',
      'value': 'properties'
    },
    {
      'name': 'Protobuf',
      'value': 'protobuf'
    },
    {
      'name': 'Python',
      'value': 'python'
    },
    {
      'name': 'R',
      'value': 'r'
    },
    {
      'name': 'RDoc',
      'value': 'rdoc'
    },
    {
      'name': 'RHTML',
      'value': 'rhtml'
    },
    {
      'name': 'Ruby',
      'value': 'ruby'
    },
    {
      'name': 'Rust',
      'value': 'rust'
    },
    {
      'name': 'SASS',
      'value': 'sass'
    },
    {
      'name': 'SCAD',
      'value': 'scad'
    },
    {
      'name': 'Scala',
      'value': 'scala'
    },
    {
      'name': 'Scheme',
      'value': 'scheme'
    },
    {
      'name': 'SCSS',
      'value': 'scss'
    },
    {
      'name': 'SH',
      'value': 'sh'
    },
    {
      'name': 'SJS',
      'value': 'sjs'
    },
    {
      'name': 'Smarty',
      'value': 'smarty'
    },
    {
      'name': 'snippets',
      'value': 'snippets'
    },
    {
      'name': 'Soy_Template',
      'value': 'soy_template'
    },
    {
      'name': 'Space',
      'value': 'space'
    },
    {
      'name': 'SQL',
      'value': 'sql'
    },
    {
      'name': 'Stylus',
      'value': 'stylus'
    },
    {
      'name': 'SVG',
      'value': 'svg'
    },
    {
      'name': 'Tcl',
      'value': 'tcl'
    },
    {
      'name': 'Tex',
      'value': 'tex'
    },


    {
      'name': 'Text',
      'value': 'text'
    },


    {
      'name': 'Textile',
      'value': 'textile'
    },


    {
      'name': 'Toml',
      'value': 'toml'
    },


    {
      'name': 'Twig',
      'value': 'twig'
    },


    {
      'name': 'Typescript',
      'value': 'typescript'
    },


    {
      'name': 'Vala',
      'value': 'vala'
    },


    {
      'name': 'VBScript',
      'value': 'vbscript'
    },


    {
      'name': 'Velocity',
      'value': 'velocity'
    },


    {
      'name': 'Verilog',
      'value': 'verilog'
    },


    {
      'name': 'VHDL',
      'value': 'vhdl'
    },


    {
      'name': 'XML',
      'value': 'xml'
    },


    {
      'name': 'XQuery',
      'value': 'xquery'
    },


    {
      'name': 'YAML',
      'value': 'yaml'
    }

  ];

  themes = [
    {
      'name': 'Ambiance',
      'value': 'ambiance'
    },
    {
      'name': 'Chaos',
      'value': 'chaos'
    },
    {
      'name': 'Chrome',
      'value': 'chrome'
    },
    {
      'name': 'Clouds',
      'value': 'clouds'
    },
    {
      'name': 'Clouds Midnight',
      'value': 'clouds_midnight'
    },
    {
      'name': 'Cobalt',
      'value': 'cobalt'
    },
    {
      'name': 'Crimson Editor',
      'value': 'crimson_editor'
    },
    {
      'name': 'Dawn',
      'value': 'dawn'
    },
    {
      'name': 'DreamWeaver',
      'value': 'dreamweaver'
    },
    {
      'name': 'Eclipse',
      'value': 'eclipse'
    },
    {
      'name': 'GitHub',
      'value': 'github'
    },
    {
      'name': 'Idle Fingers',
      'value': 'idle_fingers'
    },
    {
      'name': 'Iplastic',
      'value': 'iplastic'
    },
    {
      'name': 'Katzenmilch',
      'value': 'katzenmilch'
    },
    {
      'name': 'Kr Theme',
      'value': 'kr_theme'
    },
    {
      'name': 'Kuroir',
      'value': 'kuroir'
    },
    {
      'name': 'Merbivore',
      'value': 'merbivore'
    },
    {
      'name': 'Merbivore Soft',
      'value': 'merbivore_soft'
    },
    {
      'name': 'Mono Industrial',
      'value': 'mono_industrial'
    },
    {
      'name': 'Monokai',
      'value': 'monokai'
    },
    {
      'name': 'Pastel on Dark',
      'value': 'pastel_on_dark'
    },
    {
      'name': 'Solarized Dark',
      'value': 'solarized_dark'
    },
    {
      'name': 'Solarized Light',
      'value': 'solarized_light'
    },
    {
      'name': 'SqlServer',
      'value': 'sqlserver'
    },
    {
      'name': 'Terminal',
      'value': 'terminal'
    },
    {
      'name': 'Tomorrow',
      'value': 'tomorrow_night'
    },
    {
      'name': 'Tomorrow',
      'value': 'tomorrow'
    },
    {
      'name': 'Tomorrow Night Blue',
      'value': 'tomorrow_night_blue'
    },
    {
      'name': 'Tomorrow Night Bright',
      'value': 'tomorrow_night_bright'
    },
    {
      'name': 'Tomorrow Night Eighties',
      'value': 'tomorrow_night_eighties'
    },
    {
      'name': 'Twilight',
      'value': 'twilight'
    },
    {
      'name': 'Vibrant Ink',
      'value': 'vibrant_ink'
    },
    {
      'name': 'XCode',
      'value': 'xcode'
    }
  ];


  fontSizes = [
    {
      'name': 'Defautl',
      'value': '15px'
    },
    {
      'name': '16px',
      'value': '16px'
    },
    {
      'name': 'Big',
      'value': '20px'
    },
    {
      'name': 'Bigger',
      'value': '25px'
    }
  ];


  constructor(private router: Router,
    private nav: NavbarService,
    private activatedRoute: ActivatedRoute,
    private usuario: UsuarioService,
    private archivo: ArchivoService
  ) { }

  ngOnInit() {
    this.nav.hide();

    this.usuario.obtenerInformacionUsuario().
      subscribe(
        respuesta => {
          this.idUsuario = respuesta._id;
        }
    );

    this.language = 'javascript';
    this.theme = 'monokai';
    this.fontSize = '15px';
    this.text = '';

    console.log(this.activatedRoute.snapshot.params['id']);

    this.obtenerArchivo();
  }

  onRuleChange(e) {
    // console.log(e);
  }

  saveContent(text, lang) {
    this.nombreArchivo = this.activatedRoute.snapshot.params['nombre'];
    const blob = new Blob([text], { type: `text/${lang};charset=utf-8` });
    switch (lang) {
      case 'javascript':
        saveAs(blob, `${this.nombreArchivo}`);
        break;
      case 'php':
        saveAs(blob, `${this.nombreArchivo}`);
        break;
      case 'html':
        saveAs(blob, `${this.nombreArchivo}`);
        break;
      case 'css':
        saveAs(blob, `${this.nombreArchivo}`);
        break;
      case 'c':
        saveAs(blob, `${this.nombreArchivo}`);
        break;
      case 'c++':
        saveAs(blob, `${this.nombreArchivo}`);
        break;
      case 'java':
        saveAs(blob, `${this.nombreArchivo}`);
        break;
      case 'python':
        console.log(this.nombreArchivo);
        saveAs(blob, `${this.nombreArchivo}`);
        break;
      default:
        saveAs(blob, `${this.nombreArchivo}`);
        break;
    }
  }

  guardar(text, lang
  ) {
    const meses = ['01', '02', '03', '04', '05', '06',
    '07', '08', '09', '10', '11', '12'
  ];

    const fecha = new Date();

    const fechaActual = `${fecha.getFullYear()}-${meses[fecha.getMonth()]}-${fecha.getDate()}`;
    const idProyecto = 0;
    this.nombreArchivo = this.activatedRoute.snapshot.params['nombre'];
    const fechaCreacion = fechaActual;
    const fechaModificacion = fechaActual;
    const idCarpeta = 1;
    const idUsuarioCreador = this.idUsuario;
    const usuarios = 0;
    switch (lang) {
      case 'javascript':
        this.idTipoArchivo = 1;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'php':
        this.idTipoArchivo = 2;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'html':
        this.idTipoArchivo = 3;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'css':
        this.idTipoArchivo = 4;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'c_cpp':
        this.idTipoArchivo = 5;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'java':
        this.idTipoArchivo = 6;


        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'python':
        this.idTipoArchivo = 7;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      case 'csharp':
        this.idTipoArchivo = 8;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      case 'ruby':
        this.idTipoArchivo = 9;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      case 'typescript':
        this.idTipoArchivo = 10;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      case 'sql':
        this.idTipoArchivo = 11;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      case 'scss':
        this.idTipoArchivo = 12;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      default:
        this.idTipoArchivo = 13;

        this.archivo.crearArchivo(
          this.nombreArchivo,
          this.idTipoArchivo,
          text,
          fechaCreacion,
          fechaModificacion,
          idCarpeta,
          idProyecto,
          idUsuarioCreador,
          usuarios
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
    }
  }


  actualizar(text, lang
  ) {
    const meses = ['01', '02', '03', '04', '05', '06',
      '07', '08', '09', '10', '11', '12'
    ];

    const fecha = new Date();

    const fechaActual = `${fecha.getFullYear()}-${meses[fecha.getMonth()]}-${fecha.getDate()}`;
    const idArchivo = this.activatedRoute.snapshot.params['id'];
    const fechaModificacion = fechaActual;
    switch (lang) {
      case 'javascript':
        this.idTipoArchivo = 1;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'php':
        this.idTipoArchivo = 2;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'html':
        this.idTipoArchivo = 3;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'css':
        this.idTipoArchivo = 4;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'c_cpp':
        this.idTipoArchivo = 5;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'java':
        this.idTipoArchivo = 6;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
      case 'python':
        this.idTipoArchivo = 7;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      case 'csharp':
        this.idTipoArchivo = 8;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      case 'ruby':
        this.idTipoArchivo = 9;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      case 'typescript':
        this.idTipoArchivo = 10;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      case 'sql':
        this.idTipoArchivo = 11;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      case 'scss':
        this.idTipoArchivo = 12;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );

        break;
      default:
        this.idTipoArchivo = 13;

        this.archivo.actualizarArchivo(
          idArchivo,
          this.idTipoArchivo,
          text,
          fechaModificacion
        ).subscribe(
          respuesta => {
            if (respuesta.status === 200) {
              alert(respuesta.mensaje);
              this.router.navigate(['/']);
            }
          }
        );
        break;
    }
  }
  selectedLanguage(lang) {
    this.language = lang;
  }

  selectedTheme(sTheme) {
    this.theme = sTheme;
  }

  selectedFontSize(sFontSize) {
    this.fontSize = sFontSize;
  }

  irAHome() {
    this.router.navigate(['/']);
  }


  obtenerArchivo() {
    this.archivo.obtenerArchivo(
      this.activatedRoute.snapshot.params['id']
    ).subscribe(
      respuesta => {
        if (respuesta.status === 200) {
          console.log(respuesta);
          this.text = respuesta.mensaje;
          this.esEditable = true;
        } else if (respuesta.status === 500) {
          this.esEditable = false;
          console.log(respuesta.mensaje);
        }
      }
    );
  }

}
