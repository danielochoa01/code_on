import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AutenticacionService } from '../services/autenticacion.service';
import { UsuarioService } from '../services/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private autenticacion: AutenticacionService,
    private router: Router,
    private usuarioService: UsuarioService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.autenticacion.estaLogueado) {
      return true;
    }

    this.usuarioService.estaLogueado().subscribe(
      respuesta => {
        if (respuesta.estaLogueado) {
          this.autenticacion.setEstadoLogueado(true);
          return true;
        } else {
          this.router.navigate(['/login']);
          return false;
        }
      }
    );
  }
}
