var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var session = require('express-session');
var Models = require('./data-modeling');
var cookieParser = require('cookie-parser')
var cors = require('cors');
var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');


mongoose.Promise = Promise
mongoose.connect("mongodb://localhost:27017/code_on").
then(
    console.log("Conectado con éxito.")
);


var corsOptions = {
  origin: 'http://localhost:4200',
  credentials: true,
  optionsSuccessStatus: 200
}

var app = express();

app.use(express.static("public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser())
app.use(cors(corsOptions));
app.use(session({ secret: "asd.456", resave: true, saveUninitialized: true }));

app.post('/api/login', (req, res) => {

    var correo = req.body.correo;
    var contrasenia = req.body.contrasenia;

    Models.Usuario.findOne({ correo: correo, contrasenia: contrasenia }, function (err, data) {
        // console.log(data);
        
        if (Object.keys(data).length > 0) {
            req.session.idUsuario = data._id
            req.session.save(function () {
                console.log('Sesión guardada');
                
            });

            res.json({
                status: 200,
                mensaje: "Inicio de sesión exitoso"
            });
        } else {
            res.json({
                status: 500,
                mensaje: "Login fallido"
            });
        }

    });
});


app.get('/api/esta-logueado', (req, res) => {
    console.log('Estoy en esta-logueado');
    
    console.log(req.session.idUsuario);

    if (req.session.idUsuario > 0) {
        res.json({
            estaLogueado: true,
            mensaje: `Logueado con id de usuario ${req.session.idUsuario}`
        });
    } else {
        res.json({
            estaLogueado: false,
            mensaje: `No está logueado`
        });
    }
    
});

app.get('/api/usuario', (req, res) => {


    console.log(req.session.idUsuario);
    
    if (req.session.idUsuario) { 

        
        Models.Usuario.findOne({ _id: req.session.idUsuario }, function (err, data) {
            if (err) return console.error(err);
            // console.log(sess.correo);
            res.json({
                _id: data._id,
                nombre_completo: data.nombre_completo,
                nombre_usuario: data.nombre_usuario,
                correo: data.correo,
                fecha_nacimiento: data.fecha_nacimiento,
                fecha_registro: data.fecha_registro,
                genero: data.genero,
                url_foto: data.url_foto,
            });
        });   
    }
});

app.post('/api/file', (req, res) => {
    
    console.log(req.body);

    console.log(req.body);

    let rutaCarpeta = __dirname + '/public/directories/home'
    // console.log(rutaCarpeta);

    let rutaArchivo = `${rutaCarpeta}/${req.body.nombre_archivo}`

    let archivo = new Models.Archivo({
        id_proyecto: null, // id proyecto
        nombre_archivo: req.body.nombre_archivo,
        id_tipo_archivo: req.body.id_tipo_archivo,
        contenido_archivo: req.body.contenido,
        fecha_creacion: req.body.fecha_creacion,
        fecha_modificacion: req.body.fecha_modificacion,
        url_archivo: rutaCarpeta,
        id_carpeta_contenedora: req.body.id_carpeta_contenedora,
        id_usuario_creador: req.body.id_usuario_creador,
        usuarios: [
            null
        ],
    })
    console.log(archivo)

    archivo.save(function (error) {
        if (error) {
            console.error(error);
        } else {

            switch (req.body.id_tipo_archivo) {
                case 1:
                    fs.exists(`${rutaArchivo}.js`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.js`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.js`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 2:
                    fs.exists(`${rutaArchivo}.php`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.php`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.php`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 3:
                    fs.exists(`${rutaArchivo}.html`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.html`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.html`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 4:
                    fs.exists(`${rutaArchivo}.css`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.css`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.css`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 5:
                    fs.exists(`${rutaArchivo}.c`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.c`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.c`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 6:
                    fs.exists(`${rutaArchivo}.java`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.java`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.java`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 7:
                    fs.exists(`${rutaArchivo}.py`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.py`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.py`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 8:
                    fs.exists(`${rutaArchivo}.cs`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.cs`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.cs`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 9:
                    fs.exists(`${rutaArchivo}.rb`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.rb`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.rb`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 10:
                    fs.exists(`${rutaArchivo}.ts`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.ts`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.ts`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 11:
                    fs.exists(`${rutaArchivo}.sql`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.sql`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.sql`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                case 12:
                    fs.exists(`${rutaArchivo}.scss`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.scss`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.scss`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
                default:
                    fs.exists(`${rutaArchivo}.txt`, function (exist) {
                        if (!exist) {
                            fs.writeFile(`${rutaArchivo}.txt`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        } else {
                            fs.appendFile(`${rutaArchivo}.txt`, req.body.contenido, function (err) {
                                if (err) {
                                    return console.log(err);
                                }

                                console.log("El archivo fue creado con éxito");
                            });
                        }
                    })
                    break;
            }            
            console.log("¡Carpeta creada con éxito!");
            res.json({
                status: 200,
                "mensaje": "¡Archivo almacenado con éxito!"
            });
        }
    });    

})

app.get('/api/file', (req, res) => {


    Models.Archivo.find({ id_usuario_creador: req.session.idUsuario }).populate('id_tipo_archivo','nombre_tipo_archivo').exec(function (err, docs) {

        console.log(docs);
        res.send(docs);
        //respuesta.push(docs)


    }); 

})

app.post('/api/editor', (req, res) => {

    if (req.body.id_archivo !== '0') {
        console.log('Es mentira');

        Models.Archivo.findOne({ _id: req.body.id_archivo }, function (err, data) {

            res.json({
                status: 200,
                mensaje: data.contenido_archivo
            });

        });          
    } else {
        res.json({
            status: 500,
            mensaje: "El archivo es nuevo"
        });
    }
})


app.post('/api/actualizar', (req, res) => {


    console.log(req.body);
    

    Models.Archivo.findByIdAndUpdate(req.body.id_archivo, { 
        $set: { 
        id_tipo_archivo : req.body.id_tipo_archivo,
        contenido_archivo: req.body.contenido,
        fecha_modificacion: req.body.fecha_modificacion, } }, { new: true }, function (err, data) {
        if (err) return handleError(err);
        console.log(data);
        
            res.json({
                status: 200,
                mensaje: 'Archivo Actualizado con éxito'
            });            
    });
})


app.post('/api/directory', (req, res) => {

    console.log(req.body);

    let rutaCarpeta = __dirname + '/public/directories/home'
    console.log(rutaCarpeta);
    
    let rutaCarpetaNueva = `${rutaCarpeta}${req.body.url_carpeta}`

    let carpeta = new Models.Carpeta({
        id_proyecto: null, // id proyecto
        nombre_carpeta: req.body.nombre_carpeta,
        fecha_creacion: req.body.fecha_creacion,
        fecha_modificacion: req.body.fecha_modificacion,
        url_carpeta: rutaCarpetaNueva,
        id_carpeta_padre: req.body.id_carpeta_padre,
        id_usuario_creador: req.body.id_usuario_creador,
        usuarios: [
            null
        ],
    })
    console.log(carpeta)

    carpeta.save(function (error) {
        if (error) {
            console.error(error);
        } else {

            fs.exists(rutaCarpetaNueva, function (exist) {
                if (!exist) {
                    fs.mkdir(`${rutaCarpetaNueva}`, function () {
                        console.log('Carpeta creada con éxito');
                    })
                } else {
                    fs.mkdir(`${rutaCarpetaNueva}_copia`, function () {
                        console.log('Carpeta creada con éxito');
                    })
                }
            })

            console.log("¡Carpeta creada con éxito!");
            res.json({
                status: 200,
                "mensaje": "¡Carpeta creada con éxito!"
            });
        }
    });    
    
})

app.post('/api/delete', (req, res) => {})

app.post( '/api/register', (req, res) => {
    
    let correo = req.body.correo
    let nombreUsuario = req.body.nombreUsuario
    console.log(req.body.fechaNacimiento);
    console.log(req.body.fechaRegistro);
    
    Models.Usuario.findOne({ $or: [{ correo: correo }, { nombre_usuario: nombreUsuario }] }, function (err, data) {
        console.log(data);

        if (data == null){
            console.log('Es libre de hacer lo que quiera');

            let usuario = new Models.Usuario({
                nombre_completo: req.body.nombreCompleto,
                fecha_nacimiento: new Date(req.body.fechaNacimiento),
                nombre_usuario: req.body.nombreUsuario,
                correo: req.body.correo,
                contrasenia: req.body.contrasenia,
                url_foto: req.body.urlFoto,
                genero: req.body.genero,
                fecha_registro: new Date(req.body.fechaRegistro),
                id_plan: req.body.idPlan,
                esta_activo: req.body.estaActivo
            })
            console.log(usuario)

            usuario.save(function (error) {
                if (error) {
                    console.error(error);
                } else {
                    console.log("Usuario almacenado!");
                    res.json({
                        status: 200,
                        "mensaje": "Registro Almacenado Exitosamente"
                    });
                }
            });            
        } else {
            if (correo == data.correo) {
                console.log('El correo ya existe');            
                res.json({
                    status: 501,
                    mensaje: "El correo ya existe"
                });
            } else if (nombreUsuario == data.nombre_usuario ) {
                console.log('El usuario ya existe'); 
                res.json({
                    status: 502,
                    mensaje: "El nombre de usuario ya existe"
                });
            }
        }

    });

});

app.get('/api/logout', (req, res) => {
    req.session.destroy((err) => {
        res.json({
            status: true,
            mensaje: 'Sesión finalizada'
        })
    });
})

app.listen(3000, () => {
        console.log(`Servidor iniciado en http://localhost:3000`)
        
    }
);
