<?php

    include("../class/class_conexion.php");
    $ruta = "../archivos/"; 
    
    $idOficio = $_POST['oficio-id'];
    $codigoOficio = $_POST['codigo-oficio'];
    $remitente = $_POST['remitente'];
    $institucion = $_POST['institucion'];
    $despacho = $_POST['despacho'];
    $idUsuarioRecibido = $_POST['id-usuario-recibido'];
    $asignadoA = $_POST['asignado-a'];
    $fechaNota = $_POST['fecha-nota'];
    $departamento = $_POST['unidad-depto'];
    $resumen = $_POST['resumen'];
    $asunto = $_POST['asunto'];
    $respuesta = $_POST['respuesta'];
    $observacion = $_POST['observacion'];
    $idTipoOficio = $_POST['tipo-oficio'];
    $fechaRespuesta = $_POST['fecha-respuesta'];
    $tipoEstado = $_POST['tipo-estado'];
    $oficio = $_FILES['subir-oficio']['name'];
    $oficioTmp = $_FILES['subir-oficio']['tmp_name'];

    if ($fechaRespuesta == '') {
        $fechaRespuesta = '0000-00-00';
        
    }

    $remitente = (int)$remitente;
    $institucion = (int)$institucion;
    $despacho = (int)$despacho;
    $idUsuarioRecibido = (int)$idUsuarioRecibido;
    $asignadoA = (int)$asignadoA;
    $departamento = (int)$departamento;
    $idTipoOficio = (int)$idTipoOficio;
    $tipoEstado = (int)$tipoEstado;


    $extensiones = array('jpeg', 'jpg', 'png', 'bmp', 'pdf');
    $extension = strtolower(pathinfo($oficio, PATHINFO_EXTENSION));
    $oficioFinal = rand(1000,1000000).$oficio;    
    

    if ($fechaNota == '') {
        $mensaje = array(
            "status" => 500,
            "mensaje" => "La fecha de la nota no puede estar vacía. "
        );        
    } else if ($fechaRespuesta == '0000-00-00' && ($idTipoOficio == 1 || $idTipoOficio == 2)) {
        $mensaje = array(
            "status" => 500,
            "mensaje" => "Las invitaciones y solicitudes requieren fecha de respuesta. "
        );
    } else if (($fechaRespuesta < $fechaNota) && ($fechaRespuesta != '0000-00-00')){
        $mensaje = array(
            "status" => 500,
            "mensaje" => "La fecha de respuesta no puede ser una fecha menor a la fecha de la nota. "
        );
            
    } else if ($oficio == '') {
        $conexion = new Conexion();

        $ruta = '';  

        $sql = sprintf("CALL update_oficio_directora(
            '%s', '%s', '%s', '%s', '%s',
            '%s', '%s', '%s' ,'%s', '%s',
            '%s', '%s', '%s', '%s', '%s',
            '%s', '%s')",
            $conexion->setEscapa($idOficio),
            $conexion->setEscapa($codigoOficio),
            $conexion->setEscapa($remitente),
            $conexion->setEscapa($institucion),
            $conexion->setEscapa($despacho),
            $conexion->setEscapa($idUsuarioRecibido),
            $conexion->setEscapa($asignadoA),
            $conexion->setEscapa($fechaNota),
            $conexion->setEscapa($tipoEstado),
            $conexion->setEscapa($departamento),
            $conexion->setEscapa($resumen),
            $conexion->setEscapa($asunto),
            $conexion->setEscapa($respuesta),
            $conexion->setEscapa($observacion),        
            $conexion->setEscapa($idTipoOficio),
            $conexion->setEscapa($fechaRespuesta),
            $ruta
        );
        
        $resultado = $conexion->executeQuery($sql);
        
        if ($resultado) {
            $mensaje = array(
                'status' => 200,
                'mensaje' => '¡Información actualizada con éxito!',
            );
        } else {
            $error = $conexion->getError();
            $mensaje = array(
                'status' => 501,
                'mensaje' => $error,
            );        
        }

        $conexion->closeConnection();

    } else {

        $conexion = new Conexion();

        $ruta = $ruta.strtolower($oficioFinal); 

        if( move_uploaded_file($oficioTmp,$ruta )) {
            $documento = str_replace("../","",$ruta);      

            $sql = sprintf("CALL update_oficio_directora(
                '%s', '%s', '%s', '%s', '%s',
                '%s', '%s', '%s' ,'%s', '%s',
                '%s', '%s', '%s', '%s', '%s',
                '%s', '%s')",
                $conexion->setEscapa($idOficio),
                $conexion->setEscapa($codigoOficio),
                $conexion->setEscapa($remitente),
                $conexion->setEscapa($institucion),
                $conexion->setEscapa($despacho),
                $conexion->setEscapa($idUsuarioRecibido),
                $conexion->setEscapa($asignadoA),
                $conexion->setEscapa($fechaNota),
                $conexion->setEscapa($tipoEstado),
                $conexion->setEscapa($departamento),
                $conexion->setEscapa($resumen),
                $conexion->setEscapa($asunto),
                $conexion->setEscapa($respuesta),
                $conexion->setEscapa($observacion),        
                $conexion->setEscapa($idTipoOficio),
                $conexion->setEscapa($fechaRespuesta),
                $documento
            );
        
            $resultado = $conexion->executeQuery($sql);
            
            if ($resultado) {
                $mensaje = array(
                    'status' => 200,
                    'mensaje' => '¡Información actualizada con éxito!',
                );
            } else {
                $error = $conexion->getError();
                $mensaje = array(
                    'status' => 501,
                    'mensaje' => $error,
                );        
            }

            $conexion->closeConnection();
        }

    }

    echo json_encode($mensaje);

    