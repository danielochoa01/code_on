var mongoose = require("mongoose");
var autoIncrement = require('mongoose-auto-increment');

var db = mongoose.connection;
autoIncrement.initialize(db);

var Schema = mongoose.Schema;

var planSchema = new Schema({
    nombre_plan: String,
    costo: String,
    almacenamiento_bytes: Number,
    cantidad_usuarios: Number,
    cantidad_carpetas: Number,
    cantidad_archivos: Number

});

planSchema.plugin(autoIncrement.plugin, {
    model: 'Plan',
    startAt: 1
});

var Plan = mongoose.model('Plan', planSchema);

var usuarioSchema = new Schema({
    nombre_completo: String,
    fecha_nacimiento: Date,
    nombre_usuario: String,
    correo: String,
    contrasenia: String,
    url_foto: String,
    genero: String,
    fecha_registro: { type: Date, default: Date.now },
    id_plan: { type: Number, ref: 'Plan' },
    esta_activo: Boolean
});

usuarioSchema.plugin(autoIncrement.plugin, {
    model: 'Plan',
    startAt: 1
});

var Usuario = mongoose.model('Usuario', usuarioSchema);


var proyectoSchema = new Schema({
    nombre_proyecto: String,
    id_propietario: { type: Number, ref: 'Usuario' },
    usuarios_invitados: [
        {
            id_invitado: { type: Number, ref: 'Usuario' },
            permiso: Number
        }
    ]
});

proyectoSchema.plugin(autoIncrement.plugin, {
    model: 'Proyecto',
    startAt: 1
});

var Proyecto = mongoose.model('Proyecto', proyectoSchema);

var carpetaSchema = new Schema({
    id_proyecto: { type: Number, required: false, ref: 'Proyecto' }, // id proyecto
    nombre_carpeta: String,
    fecha_creacion: { type: Date, default: Date.now },
    fecha_modificacion: Date,
    url_carpeta: String,
    id_carpeta_padre: { type: Number, required: false, ref: 'Carpeta' },
    id_usuario_creador: { type: Number, ref: 'Usuario' },
    usuarios: [
        { type: Number, required: false, ref: 'Usuario' }
    ],
})

carpetaSchema.plugin(autoIncrement.plugin, {
    model: 'Carpeta',
    startAt: 1
});

var Carpeta = mongoose.model('Carpeta', carpetaSchema);

var tipoArchivoSchema = new Schema({
    nombre_tipo_archivo: String

});

tipoArchivoSchema.plugin(autoIncrement.plugin, {
    model: 'TipoArchivo',
    startAt: 1
});

var TipoArchivo = mongoose.model('TipoArchivo', tipoArchivoSchema);

var archivoSchema = new Schema({
    nombre_archivo: String,
    id_tipo_archivo: { type: Number, ref: 'TipoArchivo' },
    fecha_creacion: { type: Date, default: Date.now },
    fecha_modificacion: Date,
    tamanio_bytes: Number,
    contenido_archivo: String,
    url_archivo: String,
    id_carpeta_contenedora: { type: Number, ref: 'Carpeta', default: 1 },
    id_proyecto: { type: Number, required: false, ref: 'Proyecto' },
    id_usuario_creador: { type: Number, ref: 'Usuario' },
    usuarios: [
        { type: Number, required: false, ref: 'Usuario' }
    ],
});

archivoSchema.plugin(autoIncrement.plugin, {
    model: 'Archivo',
    startAt: 1
});

var Archivo = mongoose.model('Archivo', archivoSchema);

var papeleraSchema = new Schema({
    id_archivo: { type: Number, required: false, ref: 'Archivo' },
    id_carpeta: { type: Number, required: false, ref: 'Carpeta' },
    fecha_ingreso: { type: Date, default: Date.now },
})

var Papelera = mongoose.model('Papelera', papeleraSchema);

module.exports.Plan = Plan;
module.exports.Usuario = Usuario;
module.exports.Proyecto = Proyecto;
module.exports.Carpeta = Carpeta;
module.exports.TipoArchivo = TipoArchivo;
module.exports.Archivo = Archivo;
module.exports.Papelera = Papelera;