var express = require('express');
var app = express();

app.use(express.static('public'));

var port = 3500

app.listen(port, () => console.log(`listening on http://localhost:${port}`));
